## Demo a User Management System using below technology. ##


### Technology Stack: ###
Spring Boot, no-xml Spring MVC 4 web application for Servlet 3.0 environment

Thymeleaf templates

JPA 2.0 (Spring Data JPA/Hibernate)

Database (MySQL)

Java 7, Spring Security 3.2, Maven 3, Logback, Bootstrap 3.3.4, jQuery 1.11.2, etc


### How do I get set up? ###

* mvn spring-boot:run