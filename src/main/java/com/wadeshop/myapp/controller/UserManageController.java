package com.wadeshop.myapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wadeshop.myapp.persistence.entities.User;
import com.wadeshop.myapp.persistence.repositories.UserRepository;

@Controller
public class UserManageController {
    final Logger logger = LoggerFactory.getLogger(UserManageController.class);
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    /**
    * 
    * @Title: User manage page and get all user for display
    * @param model
    * @return
    */
    @RequestMapping("/userManage")
    public String userManage(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "userManage";
    }
    
    @RequestMapping(value = "/refresh",method = RequestMethod.GET)
    public String refresh(Model model) {
        Iterable<User> users = userRepository.findAll();
        model.addAttribute("users", users);
        logger.info("User manage table refereshed");
        return "userManage :: myTable";
    }

    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    @ResponseBody
    public User addUser(User user) {
        //encode password via BCrypt
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        
        userRepository.save(user);
        return user;
    }
    
    @RequestMapping(value = "/deleteUser",method = RequestMethod.GET)
    @ResponseBody
    public String deleteUser(Integer id) {
        userRepository.delete(id);
        logger.info("User has been deleted");
        return "success";
    }
    
    @RequestMapping(value = "/editUser",method = RequestMethod.POST)
    @ResponseBody
    public String editUser(User user) {
        //encode password via BCrypt
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        
        userRepository.save(user);
        
        return "success";
    }
    
    /**
    * 
    * @Title: unique user name check
    * @param name
    * @param response
    * @return
    */
    @RequestMapping(value = "/uniqueCheck", method = RequestMethod.GET)
    @ResponseBody
    public String uniqueCheck(@RequestParam("inputName") String name, @RequestParam("remoteid") Integer remoteid,HttpServletResponse response) {
        List<User> users = userRepository.findByName(name);

        if (users.size() > 0 && users.get(0).getId() != remoteid) {
            response.setStatus(403);
            logger.info("User Name is duplicate.");
            return "User Name is duplicate.";
        } 
        return "";
    }
}
