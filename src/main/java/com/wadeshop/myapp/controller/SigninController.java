package com.wadeshop.myapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SigninController {
	final Logger logger = LoggerFactory.getLogger(SigninController.class);
	
    /**
     * Sign in page
     * 
     * @Title: signin
     * @return
     */
	@RequestMapping("/signin")
	public String signin() {
		logger.info("Showing sign in page");
		return "signin";
	}
	
    /**
     * Register page
     * 
     * @Title: register
     * @return
     */
    @RequestMapping("/register")
    @ResponseBody
    public String register() {
        return "<p>Please send an email to xxx for registeration.</p>";
    }
}
