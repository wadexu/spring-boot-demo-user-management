package com.wadeshop.myapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wadeshop.myapp.persistence.repositories.UserRepository;

@Controller
public class HomeController {
    final Logger logger = LoggerFactory.getLogger(HomeController.class);
    
    @Autowired
    UserRepository userRepository;
    
    @RequestMapping("/home")
    public String home(Model model) {
        logger.info("Showing home page");
        return "home";
    }
    
    @RequestMapping("/about")
    @ResponseBody
    public String about() {
        return "<p>About author Wade Xu: A Senior Test Engineer in a MNC.</p>";
    }

    @RequestMapping("/contact")
    @ResponseBody
    public String contact() {
        return "<p>XXX Corporation <br> 123 Main St <br>"
                + "www.cnblogs.com/wade-xu/ <br>Mail:<a href=\"mailto:xjs-1985@163.com\">xjs-1985@163.com</a></p>";

    }
    
}
