package com.wadeshop.myapp.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;


@Configuration
@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService customUserDetailsService;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//    	auth
//         	.inMemoryAuthentication()
//         		.withUser("user").password("user").roles("USER").and()
//         		.withUser("admin").password("admin").roles("ADMIN");
//    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder);
    }

	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()  //disable csrf
            .authorizeRequests()
            .antMatchers("/css/**","/js/**","/fonts/**","/img/**","/home").permitAll()
            .antMatchers("/userManage").hasRole("ADMIN")
            .anyRequest().permitAll()
	            .and()
	        .formLogin()
	            .loginPage("/signin").permitAll()
	            .defaultSuccessUrl("/home")
	            .and()
            .logout()
            	.logoutUrl("/logout")
                .logoutSuccessUrl("/home")
                .deleteCookies("JSESSIONID")
                .permitAll();
    }

    
//  @Override
//  protected void configure(HttpSecurity http) throws Exception {
//      http
//          .authorizeRequests()
//          .antMatchers("/**", "/styles/**", "/scripts/**", "/partials/**")
//          .permitAll()
//          .anyRequest()
//          .authenticated()
//              .and()
//          .formLogin().and().addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
//          .csrf().csrfTokenRepository(csrfTokenRepository())
//              .and()
//          .logout();
//  }
    
//    private CsrfTokenRepository csrfTokenRepository() {
//        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
//        repository.setHeaderName("X-XSRF-TOKEN");
//        return repository;
//    }

    @Bean(name="passwordEncoder")
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
}