package com.wadeshop.myapp.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @Description: TODO
 * @author wadexu
 *
 * @updateUser
 * @updateDate
 */
@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private int id;
    
    @Column(name = "name")
    @NotEmpty(message = "{validation.not-empty.message}")
    private String name;

    @Column(name = "password")
    @NotEmpty(message = "{validation.not-empty.message}")
    private String password;

    @Column(name = "email")
    @Email(message = "{validation.email.message}")
    private String email;

    @Column(name = "role")
    private String role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

//    public RoleEnum getRole() {
//        return role;
//    }
//
//    public void setRole(RoleEnum role) {
//        this.role = role;
//    }
}    