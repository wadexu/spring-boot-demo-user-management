package com.wadeshop.myapp.persistence.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.wadeshop.myapp.persistence.entities.User;

@RepositoryRestResource
public interface UserRepository extends CrudRepository<User, Integer> {

	List<User> findByName(@Param("name") String userName);

}
