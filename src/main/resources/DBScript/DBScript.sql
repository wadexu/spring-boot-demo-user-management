-- --------------------------------------------------------
-- Host:                         10.10.201.1
-- Server version:               5.1.66-community - MySQL Community Server (GPL)
-- Server OS:                    unknown-linux-gnu
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-11-26 16:09:01
-- --------------------------------------------------------

-- Dumping database structure for wadexu
CREATE DATABASE IF NOT EXISTS `wadexu` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `wadexu`;


-- Dumping structure for table wadexu.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(20) DEFAULT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Dumping data for table wadexu.users: 4 rows
INSERT INTO `users` (`id`, `name`, `password`, `email`, `role`) VALUES
	(1, 'admin', '$2a$10$Ib/A2ua0G7GO8luRGJmaVusiIMkDDdRg/q6NLhuudpytzkLEKj2Dm', 'wade.xu@xxx.com', 'ADMIN'),
	(2, 'wade', '$2a$10$hTlulYor76cuJWkHQMFXjuoJ/uKMIZ.PuNaZ3r.NAenB.XMwhW/Ny', 'wade.xu@xxx.com', 'ADMIN'),
	(3, 'user', '$2a$10$CFUkat/s04CDp.EsNAJw9uYxeVFD0VdDjPecqGyDRi82R5QZ/Y4vq', 'user@163.com', 'USER'),
	(4, 'test', '$2a$10$bkrroKl.0dV/G7fJV6XbW.I1/jPPjWQWui5uSZTS5BqxTn3TneOtG', 'test@163.com', 'USER');
