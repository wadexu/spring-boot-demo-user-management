$(document)
		.ready(
				function() {

					// create a user
					$("#create").click(function() {

						var inputName = $("#inputName").val();
						var inputPWD = $("#inputPWD").val();
						var inputEmail = $("#inputEmail").val();
						var role = $("#role").val();
//						var csrf = $("#csrf").val();
						
					//validate form
					$('#addForm').validator('validate');
					if(!$("#addForm button").hasClass('disabled')){
						    
						$.ajax({
							url : 'addUser',
							data : {
								name : inputName,
								password : inputPWD,
								email : inputEmail,
								role : role
//								_csrf : csrf
							},
							type : 'post',
							dataType : 'json',
							success : function(data) {
								
//								var tr = "<tr><td th:text=\""+ data.name +"\">Onions</td><td th:text=\"" + data.password + "\">241</td><td th:text=\"" +
//							    data.email+"\">test</td><td th:text=\"" + data.role + "\">role</td><td width=\"8%\" style=\"text-align:center\"><a href=\"data/configure\" class=\"glyphicon glyphicon-edit\" style=\"color:black\"></a><a href=\"data/configure\" class=\"glyphicon glyphicon-remove\" style=\"color:black\"></a></td></tr>";
//
//							    $("#myTable tbody").append(tr);
								
							    $("#refresh").click();
							    $('#myModal').modal('hide');
							},
							error : function(data) {
								alert(data.status);
								alert("Error");
							}
						});
					}
						
					});
					
					//add user
					$("#addUser").click(function() {
						
						//reset field value
						reset();
						
						//reset validator
						$('#addForm').validator('destroy');
						$('#addForm').validator();
						
						//show add user modal
						$('#save').hide();
						$('#create').show()
						$('#myModalLabel').text('Add User');
						$('#myModal').modal('show');
					
						//reset remoteid
						$("#inputName").attr("remoteid", 0);
					});

					//referesh page
					$("#refresh").click(function() {
						var url = "refresh";
						$("#tablediv").load(url);
					});
				});
		

var id = '';

function deleteConfirm(obj) {
	id = $(obj).parents('tr').children("input").val();
    $('#confirmDialog').modal('show');
	
}

function deleteUser() {

	$.ajax({
		url : 'deleteUser',
		data : {
			id : id

		},
		type : 'get',
		success : function(data) {

			$("#refresh").click();
		    $('#confirmDialog').modal('hide');
		},
		error : function(data) {
			alert(data.status);
			alert("Error");
		}
	});
}

function reset() {
	$("#inputName").val('');
	$("#inputPWD").val('');
	$("#inputEmail").val('');
	$("#role").val('');

}

var record_id = '';

function editDialog(obj) {
	record_id = $(obj).parents('tr').children("input").val();
	var list = $(obj).parents('tr').children("td");
	
	$("#inputName").attr("remoteid",record_id);
	
	//reset validator
	$('#addForm').validator('destroy');
	$('#addForm').validator();	

	//show edit user modal
	$('#save').show()
	$('#create').hide()
	$('#myModalLabel').text('Edit User');
    $('#myModal').modal('show');
    
    //copy value except password
    $("#inputName").val(list[0].innerText);
	$("#inputEmail").val(list[2].innerText);
	$("#role").val(list[3].innerText);
//	$("#inputPWD").val(list[1].innerText);
	$("#inputPWD").val(''); //must reset password when edit since encrypt
	
}

function edit() {
	var inputName = $("#inputName").val();
	var inputPWD = $("#inputPWD").val();
	var inputEmail = $("#inputEmail").val();
	var role = $("#role").val();
//    var csrf = $("#csrf").val();


	// validate form
	$('#addForm').validator('validate');
	if (!$("#addForm button").hasClass('disabled')) {

		$.ajax({
			url : 'editUser',
			data : {
				id : record_id,
				name : inputName,
				password : inputPWD,
				email : inputEmail,
				role : role
			// _csrf : csrf

			},
			type : 'post',
			success : function(data) {
				$("#refresh").click();
				$('#myModal').modal('hide');
			},
			error : function(data) {
				alert(data.status);
				alert("Error");
			}
		});
	}
}

